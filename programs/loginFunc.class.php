<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */


define('AUTH_BASIC_NOT_LOGGED', '0');
define('AUTH_BASIC_IN_PROGRESS', '1');
define('AUTH_BASIC_LOGGED', '2');


bab_functionality::includeFile('PortalAuthentication');






class Func_PortalAuthentication_AuthBasic extends Func_PortalAuthentication
{

	public function getDescription()
	{
		return bab_translate("Basic authentication");
	}

	protected function systemExit($value = 0)
	{
	    exit($value);
	}
	
	public function authenticate()
	{
	    header('WWW-Authenticate: Basic realm="' . $GLOBALS['babSiteName'] . '"');
	    header('HTTP/1.0 401 Unauthorized');
	    echo 'HTTP/1.0 401 Unauthorized';
	    $this->systemExit();
	}

	public function login()
	{
		if ($this->isLogged()) {
			return true;
		}
		
		require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';
		

		$sLogin		= isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : bab_gp('authUser', '');
		$sPassword	= isset($_SERVER['PHP_AUTH_PW']) ? $_SERVER['PHP_AUTH_PW'] : bab_gp('authPwd', '');
		
		if ($sLogin && $sPassword) {
			$iIdUser = $this->authenticateUser($sLogin, $sPassword);
			
			if (null === $iIdUser) {
				header('HTTP/1.0 401 Unauthorized');
				foreach($this->errorMessages as $message) {
					echo $message." \n";
				}
				$this->systemExit();
			}
			
			if($this->userCanLogin($iIdUser)) {
				$this->setUserSession($iIdUser);
				return true;
			}
		}
    	$this->authenticate();
		return false;
	}
	
	public function logout()
	{
		require_once $GLOBALS['babInstallPath'] . 'utilit/loginIncl.php';
		bab_logout();
		header('HTTP/1.1 401 Unauthorized');
		header('Location:'. $GLOBALS['babUrlScript']);
		$this->systemExit();
	}
}
