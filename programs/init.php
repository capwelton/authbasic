<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */



function AuthBasic_onDeleteAddon()
{
	require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
	
	$addonInfo = bab_getAddonInfosInstance('AuthBasic');
	$addonPhpPath = $addonInfo->getPhpPath();
	
	$oFunctionalities = new bab_functionalities();
	
	if (false !== $oFunctionalities->unregister('PortalAuthentication/AuthBasic', $addonPhpPath . 'loginFunc.class.php')) {
		return true;
	}
	return false;
}

function AuthBasic_upgrade($sVersionBase, $sVersionIni)
{
	require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
	
	$addonInfo = bab_getAddonInfosInstance('AuthBasic');
	$addonPhpPath = $addonInfo->getPhpPath();
	
	$oFunctionalities = new bab_functionalities();

	if (false !== $oFunctionalities->register('PortalAuthentication/AuthBasic', $addonPhpPath . 'loginFunc.class.php')) {
		return true;
	}
	return false;
}

function AuthBasic_onPackageAddon()
{
	AuthBasic_upgrade(0, 0);
	return true;
}
