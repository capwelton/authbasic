; <?php/*
[general]
name                            ="AuthBasic"
version                         ="0.3.1"
encoding                        ="UTF-8"
mysql_character_set_database    ="latin1,utf8"
description                     ="Basic authentication. In the context of an HTTP transaction, the basic access authentication is a method designed to allow a web browser, or other client program, to provide credentials"
description.fr                  ="Module d'authentification HTTP"
long_description.fr             ="README.md"
delete                          =1
longdesc                        =""
ov_version                      ="6.6.91"
php_version                     ="4.1.2"
addon_access_control            =0
author                          ="samuel zebina ( samuel.zebina@cantico.fr )"
icon                            ="icon.png"
;*/?>