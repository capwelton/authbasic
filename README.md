## Authentification HTTP ##

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/597010fa-6dfa-4476-b968-f9477cc2dd23/big.png)](https://insight.sensiolabs.com/projects/597010fa-6dfa-4476-b968-f9477cc2dd23)

Authentification HTTP Basique, permet de s'authentifier à Ovidentia en utilisant le protocole d'authentification HTTP avec la méthode basique.